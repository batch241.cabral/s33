//Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos")
    .then((res) => res.json())
    .then((json) => {
        console.log(json);
        //Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.
        const titles = json.map((item) => item.title);
        console.log(titles);
    });

//Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1")
    .then((res) => res.json())
    .then((json) => {
        console.log(json);
        //Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
        console.log(
            `Title: ${json.title}, Status: ${json.completed ? "true" : "false"}`
        );
    });

//Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "POST",
    headers: {
        "Content-type": "application/json",
    },
    body: JSON.stringify({
        "userId": 1,
        "title": "To do list created",
        "completed": false,
    }),
})
    .then((res) => res.json())
    .then((json) => console.log(json));

//Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        "Content-type": "application/json",
    },
    body: JSON.stringify({
        "userId": 1,
        "title": "To do list updated",
        "completed": true,
    }),
})
    .then((res) => res.json())
    .then((json) => console.log(json));

//Update a to do list item by changing the data structure to contain the following properties:
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PUT",
    headers: {
        "Content-type": "application/json",
    },
    body: JSON.stringify({
        "userId": 1,
        "title": "To do list updated",
        "Description": "simple task",
        "Date Completed": "01/01/01",
        "status": "Not Completed",
    }),
})
    .then((res) => res.json())
    .then((json) => console.log(json));

//Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "PATCH",
    headers: {
        "Content-type": "application/json",
    },
    body: JSON.stringify({
        "userId": 1,
        "title": "To do list updated one more time",
    }),
})
    .then((res) => res.json())
    .then((json) => console.log(json));

//Update a to do list item by changing the status to complete and add a date when the status was changed.
const updateTodo = (id, completed, date) => {
    fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ completed, date }),
    })
        .then((res) => res.json())
        .then((data) => {
            console.log(data);
        });
};
updateTodo(1, true, new Date());

//Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
const deleteTodo = (id) => {
    fetch(`https://jsonplaceholder.typicode.com/todos/${id}`, {
        method: "DELETE",
    });
};
deleteTodo(1);
