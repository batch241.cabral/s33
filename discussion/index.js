console.log(fetch("https://jsonplaceholder.typicode.com/posts"));
fetch("https://jsonplaceholder.typicode.com/posts").then((res) =>
    console.log(res.status)
);

fetch("https://jsonplaceholder.typicode.com/posts")
    .then((res) => res.json())
    .then((JSON) => console.log(JSON));

async function fetchData() {
    let result = await fetch("https://jsonplaceholder.typicode.com/posts");
    console.log(result);
    console.log(typeof result);
    console.log(result.body);

    let json = await result.json();
    console.log(json);
}
fetchData();

fetch("https://jsonplaceholder.typicode.com/posts", {
    method: "POST",
    headers: {
        "Content-type": "application/json",
    },
    body: JSON.stringify({
        "userId": 1,
        "title": "Create Post",
        "body": "Create post file",
    }),
})
    .then((res) => res.json())
    .then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/posts/16", {
    method: "PUT",
    headers: {
        "Content-type": "application/json",
    },
    body: JSON.stringify({
        "userId": 1,
        "title": "Update",
        "body": "UpdateBody",
    }),
})
    .then((res) => res.json())
    .then((json) => console.log(json));
